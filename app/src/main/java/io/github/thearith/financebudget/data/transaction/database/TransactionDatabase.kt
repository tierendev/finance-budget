package io.github.thearith.financebudget.data.transaction.database

import io.github.thearith.financebudget.data.transaction.TransactionDataSource
import io.github.thearith.financebudget.models.Transaction
import io.reactivex.Completable
import javax.inject.Inject

class TransactionDatabase @Inject constructor() : TransactionDataSource {

    override fun saveTransaction(transaction: Transaction): Completable {
        return Completable.fromCallable {
            TransactionModel(transaction).save()
        }
    }
}
