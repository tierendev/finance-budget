package io.github.thearith.financebudget.di

import dagger.Component
import io.github.thearith.financebudget.di.modules.AppModule
import io.github.thearith.financebudget.di.modules.DataModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    DataModule::class
])
interface AppComponent : AppServiceProvider {

    @Component.Builder
    interface Builder {
        fun application(appModule: AppModule): Builder
        fun build(): AppComponent
    }
}