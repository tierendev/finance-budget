package io.github.thearith.financebudget.features.main

import android.app.AlertDialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import io.github.thearith.financebudget.BaseActivity
import io.github.thearith.financebudget.R
import io.github.thearith.financebudget.data.transaction.TransactionRepository
import io.github.thearith.financebudget.databinding.ActivityMainBinding
import io.github.thearith.financebudget.features.input.TransactionInputView
import javax.inject.Inject

class MainActivity : BaseActivity() {

    @Inject lateinit var transactionRepository: TransactionRepository

    private val mainViewModel = MainViewModel(
            transactionRepository,
            this::openTransactionDialog,
            this::closeTransactionDialog
    )

    private var transactionDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpDialog()
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)
                .model = mainViewModel
    }

    private fun setUpDialog() {
        transactionDialog = AlertDialog.Builder(this)
                .apply {
                    val view = TransactionInputView(context, viewModel = mainViewModel)
                    setView(view)
                }
                .create()
    }

    private fun openTransactionDialog() {
        val transactionDialog = transactionDialog ?: return
        if(!transactionDialog.isShowing) {
            transactionDialog.show()
        }
    }

    private fun closeTransactionDialog() {
        if(transactionDialog?.isShowing == true) {
            transactionDialog?.dismiss()
        }
    }

}
