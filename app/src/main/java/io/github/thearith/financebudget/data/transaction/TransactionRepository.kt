package io.github.thearith.financebudget.data.transaction

import io.github.thearith.financebudget.data.transaction.database.TransactionDatabase
import io.github.thearith.financebudget.models.Transaction
import io.reactivex.Completable
import javax.inject.Inject

interface TransactionRepository {
    fun saveTransaction(transaction: Transaction): Completable
}

class TransactionRepositoryImpl @Inject constructor(
        private val database: TransactionDatabase
) : TransactionRepository {

    override fun saveTransaction(transaction: Transaction): Completable {
        return database.saveTransaction(transaction)
    }

}