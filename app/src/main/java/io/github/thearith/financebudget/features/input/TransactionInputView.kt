package io.github.thearith.financebudget.features.input

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.*
import io.github.thearith.financebudget.R
import io.github.thearith.financebudget.extensions.bindTo
import io.github.thearith.financebudget.extensions.bindView
import io.github.thearith.financebudget.extensions.scheduleOn
import io.github.thearith.financebudget.models.Transaction
import io.github.thearith.financebudget.utils.randomLocalId
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber

private const val DEBOUNCE_BUTTON_MS = 500L

class TransactionInputView(
        context: Context,
        attrs: AttributeSet? = null,
        private val viewModel: TransactionViewModel
) : LinearLayout(context, attrs) {

    private val inputAmount by bindView<EditText>(R.id.input_amount)
    private val inputDescription by bindView<EditText>(R.id.input_description)
    private val inputCategory by bindView<Spinner>(R.id.input_category)
    private val inputSubmit by bindView<Button>(R.id.input_submit)

    private val categories = context.resources.getStringArray(R.array.transaction_category)

    private val disposables = CompositeDisposable()

    //TODO: fix event stream when finish everything
    // Rx events
//    private val categoryStream by lazy(NONE) {
//        RxAdapterView.itemSelections(inputCategory)
//                .map { pos -> categories[pos] }
//    }
//    private val amountStream by lazy(NONE) {
//        RxTextView.textChanges(inputAmount)
//                .filter { it.isNotEmpty() }
//                .map { it.toString().toFloat() }
//    }
//    private val descriptionStream by lazy(NONE) {
//        RxTextView.textChanges(inputDescription)
//                .map {it.toString() }
//    }
//    private val submitClickStream by lazy(NONE) { RxView.clicks(inputSubmit) }

    init {
        View.inflate(context, R.layout.v_input_transaction, this)
        orientation = VERTICAL
        val padding = resources.getDimensionPixelSize(R.dimen.grid_size_x2)
        setPadding(padding, padding, padding, padding)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        populateSpinner()
        setUpSubmitButton()

    }

    private fun populateSpinner() {
        inputCategory.adapter = ArrayAdapter<String>(
                context,
                R.layout.v_transaction_category,
                R.id.category,
                categories
        )
    }

    //TODO: fix event stream when finish everything
    //TODO: have error form when inputAmount is empty or 0
//    private fun setUpStreams() {
//        val inputStream: Observable<Transaction> = Observable.combineLatest(
//                categoryStream,
//                amountStream,
//                descriptionStream,
//                Function3 { category, amount, description ->
//                    Transaction(category, amount, description)
//                }
//        )
//
//        val clickStream = submitClickStream
//                .throttleFirst(DEBOUNCE_BUTTON_MS, TimeUnit.MILLISECONDS)
//
//        clickStream
//                .switchMap { inputStream }
//                .subscribe(
//                        { transaction ->
//                            viewModel.saveTransaction(transaction)
//                            Toast.makeText(context, "Save Successfully", Toast.LENGTH_SHORT)
//                                    .show()
//                        },
//                        { Timber.e("Error with saving transaction") }
//                )
//                .bindTo(disposables)
//    }

    private fun setUpSubmitButton() {
        inputSubmit.setOnClickListener {
            val newId = randomLocalId()
            val category = inputCategory.selectedItem.toString()
            val amount = inputAmount.text.toString().toFloat()
            val description = inputDescription.toString()
            val transaction = Transaction(newId, category, amount, description)
            viewModel.saveTransaction(transaction)
                    .scheduleOn()
                    .subscribe(
                            {
                                Toast.makeText(context, R.string.v_transaction_successful, Toast.LENGTH_SHORT)
                                        .show()
                            },
                            { Timber.e(it) }
                    )
                    .bindTo(disposables)
        }
    }

    private fun clearForm() {
        inputCategory.setSelection(0)
        inputAmount.text.clear()
        inputDescription.text.clear()
    }

    override fun onDetachedFromWindow() {
        disposables.clear()
        clearForm()
        super.onDetachedFromWindow()
    }
}