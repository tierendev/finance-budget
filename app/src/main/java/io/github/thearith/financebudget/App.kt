package io.github.thearith.financebudget

import android.app.Application
import com.activeandroid.ActiveAndroid
import com.activeandroid.Configuration
import io.github.thearith.financebudget.di.AppServiceProvider
import io.github.thearith.financebudget.di.DaggerAppComponent
import io.github.thearith.financebudget.di.modules.AppModule

class App : Application() {

    val appComponent: AppServiceProvider = DaggerAppComponent.builder()
            .application(AppModule(this))
            .build()

    override fun onCreate() {
        super.onCreate()
        initializeDb()
    }

    private fun initializeDb() {
        ActiveAndroid.initialize(Configuration.Builder(this).create())
    }
}