package io.github.thearith.financebudget

import android.support.v7.app.AppCompatActivity
import io.github.thearith.financebudget.di.AppServiceProvider

abstract class BaseActivity : AppCompatActivity() {

    val appComponent: AppServiceProvider
        get() = (application as App).appComponent
}