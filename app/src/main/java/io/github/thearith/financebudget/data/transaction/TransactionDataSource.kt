package io.github.thearith.financebudget.data.transaction

import io.github.thearith.financebudget.models.Transaction
import io.reactivex.Completable

interface TransactionDataSource {
    fun saveTransaction(transaction: Transaction): Completable
}