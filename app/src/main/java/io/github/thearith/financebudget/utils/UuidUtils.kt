package io.github.thearith.financebudget.utils

import java.util.UUID

const val LOCAL = "LOCAL_"

fun randomUuid() = UUID.randomUUID().toString()

fun randomLocalId() = LOCAL + UUID.randomUUID().toString()