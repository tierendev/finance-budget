//package io.github.thearith.financebudget.data.transaction.database
//
//import com.activeandroid.Model
//import com.activeandroid.annotation.Column
//import com.activeandroid.annotation.Table
//
//private const val TABLE_NAME = "transaction"
//private const val COLUMN_ID = "id"
//private const val COLUMN_CATEGORY = "category"
//private const val COLUMN_AMOUNT = "amount"
//private const val COLUMN_DESCRIPTION = "description"
//
//@Table(name  = TABLE_NAME)
//data class TransactionModel(
//        @JvmField
//        @Column(name = COLUMN_ID,
//                index = true,
//                unique = true,
//                onUniqueConflict = Column.ConflictAction.REPLACE
//        )
//        var id: String = "",
//
//        @JvmField
//        @Column(name = COLUMN_CATEGORY)
//        var category: String = "",
//
//        @JvmField
//        @Column(name = COLUMN_AMOUNT)
//        var amount: Float = 0F,
//
//        @JvmField
//        @Column(name = COLUMN_DESCRIPTION)
//        var description: String = ""
//
//) : Model()