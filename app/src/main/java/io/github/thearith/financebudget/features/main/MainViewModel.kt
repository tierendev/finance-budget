package io.github.thearith.financebudget.features.main

import io.github.thearith.financebudget.R
import io.github.thearith.financebudget.data.transaction.TransactionRepository
import io.github.thearith.financebudget.features.input.TransactionViewModel
import io.github.thearith.financebudget.models.Transaction
import io.reactivex.Completable

class MainViewModel(
        private val transitionRepo: TransactionRepository,
        val openTransactionDialog: () -> Unit,
        private val closeTransactionDialog: () -> Unit
) : TransactionViewModel {

    val title = R.string.app_name
    val menu = R.menu.menu_main

    fun onMenuAction(id: Int) {

    }

    override fun saveTransaction(transaction: Transaction): Completable {
        return transitionRepo.saveTransaction(transaction)
    }

}