package io.github.thearith.financebudget.data.transaction.database;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import io.github.thearith.financebudget.models.Transaction;

@Table(name = "transaction")
public class TransactionModel extends Model {

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_CATEGORY = "category";
    private static final String COLUMN_AMOUNT = "amount";
    private static final String COLUMN_DESCRIPTION = "description";

//    @Column(name = COLUMN_ID,
//            index = true,
//            unique = true,
//            onUniqueConflict = Column.ConflictAction.REPLACE
//    )
//    String id;

    @Column(name = COLUMN_CATEGORY)
    String category;
//
//    @Column(name = COLUMN_AMOUNT)
//    double amount;
//
//    @Column(name = COLUMN_DESCRIPTION)
//    String description;

    @SuppressWarnings("unused")
    TransactionModel() {
        super();
    }

    TransactionModel(Transaction transaction) {
        super();
//        id = transaction.getId();
        category = transaction.getCategory();
//        amount = transaction.getAmount();
//        description = transaction.getDescription();
    }

}