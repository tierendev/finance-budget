package io.github.thearith.financebudget.di.modules

import dagger.Binds
import dagger.Module
import io.github.thearith.financebudget.data.transaction.TransactionRepository
import io.github.thearith.financebudget.data.transaction.TransactionRepositoryImpl

@Module
internal interface DataModule {

    @Binds
    fun providesTransactionRepository(repo: TransactionRepositoryImpl): TransactionRepository

}