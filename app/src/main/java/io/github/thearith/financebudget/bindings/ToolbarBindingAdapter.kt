package io.github.thearith.financebudget.bindings

import android.databinding.BindingAdapter
import android.support.annotation.MenuRes
import android.support.annotation.StringRes
import android.support.v7.widget.Toolbar

@BindingAdapter("toolbarTitle")
fun setToolbarTitle(toolbar: Toolbar, @StringRes titleRes: Int) {
    toolbar.setTitle(titleRes)
}

@BindingAdapter("menu")
fun setMenu(toolbar: Toolbar, @MenuRes menu: Int?) {
    toolbar.menu.clear()
    if(menu == null || menu == 0) return
    toolbar.inflateMenu(menu)
}

@BindingAdapter("onMenuClick")
fun setMenuClickListener(toolbar: Toolbar, clickListener: OnMenuClickListener?) {
    toolbar.setOnMenuItemClickListener { item ->
        clickListener?.onMenuClick(item.itemId)
        true
    }
}

interface OnMenuClickListener {
    fun onMenuClick(id: Int)
}