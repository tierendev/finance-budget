package io.github.thearith.financebudget.extensions

import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

fun Disposable.bindTo(disposables: CompositeDisposable) = apply { disposables.add(this) }

fun <T> Observable<T>.scheduleOn(): Observable<T> = this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

fun Completable.scheduleOn(): Completable = this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())