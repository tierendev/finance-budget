package io.github.thearith.financebudget.features.input

import io.github.thearith.financebudget.models.Transaction
import io.reactivex.Completable

interface TransactionViewModel {
    fun saveTransaction(transaction: Transaction): Completable
}