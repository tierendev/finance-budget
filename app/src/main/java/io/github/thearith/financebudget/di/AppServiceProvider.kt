package io.github.thearith.financebudget.di

import io.github.thearith.financebudget.App

interface AppServiceProvider {

    fun inject(app: App)
}