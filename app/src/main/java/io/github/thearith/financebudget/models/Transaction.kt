package io.github.thearith.financebudget.models

data class Transaction(
        val id: String,
        val category: String,
        val amount: Float,
        val description: String
)